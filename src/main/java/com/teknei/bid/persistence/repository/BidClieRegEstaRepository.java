package com.teknei.bid.persistence.repository;

import com.teknei.bid.persistence.entities.BidClieRegEsta;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BidClieRegEstaRepository extends JpaRepository<BidClieRegEsta, Long> {

    List<BidClieRegEsta> findAllByIdClie(Long idClie);

    BidClieRegEsta findByIdClieAndIdEstaProc(Long idClie, Long idEstaProc);



}