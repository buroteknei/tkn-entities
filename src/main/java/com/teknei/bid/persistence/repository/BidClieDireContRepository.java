package com.teknei.bid.persistence.repository;

import com.teknei.bid.persistence.entities.BidClieDireCont;
import com.teknei.bid.persistence.entities.BidClieDireContPK;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BidClieDireContRepository extends JpaRepository<BidClieDireCont, BidClieDireContPK> {

    BidClieDireCont findTopByIdClieAndIdEsta(Long idClie, Integer idEsta);

}