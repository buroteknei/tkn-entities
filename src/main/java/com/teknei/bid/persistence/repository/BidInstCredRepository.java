package com.teknei.bid.persistence.repository;

import com.teknei.bid.persistence.entities.BidInstCred;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BidInstCredRepository extends JpaRepository<BidInstCred, Long> {
}
