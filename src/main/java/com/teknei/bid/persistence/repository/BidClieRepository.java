package com.teknei.bid.persistence.repository;

import com.teknei.bid.persistence.entities.BidClie;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Amaro on 28/07/2017.
 */
public interface BidClieRepository extends JpaRepository<BidClie, Long> {
}
