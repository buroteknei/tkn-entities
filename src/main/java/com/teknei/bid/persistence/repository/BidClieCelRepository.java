package com.teknei.bid.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.teknei.bid.persistence.entities.BidClieCel;

public interface BidClieCelRepository extends JpaRepository<BidClieCel, Long> {

	    BidClieCel findByIdClie(Long idClie);
}

