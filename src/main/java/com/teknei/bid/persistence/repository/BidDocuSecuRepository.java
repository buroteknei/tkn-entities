package com.teknei.bid.persistence.repository;

import com.teknei.bid.persistence.entities.BidDocuSecu;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BidDocuSecuRepository extends JpaRepository<BidDocuSecu, Long> {
}
