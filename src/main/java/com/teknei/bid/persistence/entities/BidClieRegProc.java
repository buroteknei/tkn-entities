package com.teknei.bid.persistence.entities;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "bid_clie_reg_proc", schema = "bid", catalog = "bid")
@IdClass(BidClieRegProcPK.class)
public class BidClieRegProc {
    private long idClie;
    private String curp;
    private Boolean regIne;
    private Timestamp fchRegIne;
    private Boolean regFaci;
    private Timestamp fchRegFaci;
    private Boolean regDomi;
    private Timestamp fchRegDomi;
    private Boolean regDact;
    private Timestamp fchRegDact;
    private Boolean regCont;
    private Timestamp fchRegCont;
    private int idTipo;
    private int idEsta;
    private String usrCrea;
    private Timestamp fchCrea;
    private String usrModi;
    private Timestamp fchModi;
    private String usrOpeCrea;
    private String usrOpeModi;

    @Id
    @Column(name = "id_clie")
    public long getIdClie() {
        return idClie;
    }

    public void setIdClie(long idClie) {
        this.idClie = idClie;
    }

    @Id
    @Column(name = "curp")
    public String getCurp() {
        return curp;
    }

    public void setCurp(String curp) {
        this.curp = curp;
    }

    @Basic
    @Column(name = "reg_ine")
    public Boolean getRegIne() {
        return regIne;
    }

    public void setRegIne(Boolean regIne) {
        this.regIne = regIne;
    }

    @Basic
    @Column(name = "fch_reg_ine")
    public Timestamp getFchRegIne() {
        return fchRegIne;
    }

    public void setFchRegIne(Timestamp fchRegIne) {
        this.fchRegIne = fchRegIne;
    }

    @Basic
    @Column(name = "reg_faci")
    public Boolean getRegFaci() {
        return regFaci;
    }

    public void setRegFaci(Boolean regFaci) {
        this.regFaci = regFaci;
    }

    @Basic
    @Column(name = "fch_reg_faci")
    public Timestamp getFchRegFaci() {
        return fchRegFaci;
    }

    public void setFchRegFaci(Timestamp fchRegFaci) {
        this.fchRegFaci = fchRegFaci;
    }

    @Basic
    @Column(name = "reg_domi")
    public Boolean getRegDomi() {
        return regDomi;
    }

    public void setRegDomi(Boolean regDomi) {
        this.regDomi = regDomi;
    }

    @Basic
    @Column(name = "fch_reg_domi")
    public Timestamp getFchRegDomi() {
        return fchRegDomi;
    }

    public void setFchRegDomi(Timestamp fchRegDomi) {
        this.fchRegDomi = fchRegDomi;
    }

    @Basic
    @Column(name = "reg_dact")
    public Boolean getRegDact() {
        return regDact;
    }

    public void setRegDact(Boolean regDact) {
        this.regDact = regDact;
    }

    @Basic
    @Column(name = "fch_reg_dact")
    public Timestamp getFchRegDact() {
        return fchRegDact;
    }

    public void setFchRegDact(Timestamp fchRegDact) {
        this.fchRegDact = fchRegDact;
    }

    @Basic
    @Column(name = "reg_cont")
    public Boolean getRegCont() {
        return regCont;
    }

    public void setRegCont(Boolean regCont) {
        this.regCont = regCont;
    }

    @Basic
    @Column(name = "fch_reg_cont")
    public Timestamp getFchRegCont() {
        return fchRegCont;
    }

    public void setFchRegCont(Timestamp fchRegCont) {
        this.fchRegCont = fchRegCont;
    }

    @Basic
    @Column(name = "id_tipo")
    public int getIdTipo() {
        return idTipo;
    }

    public void setIdTipo(int idTipo) {
        this.idTipo = idTipo;
    }

    @Basic
    @Column(name = "id_esta")
    public int getIdEsta() {
        return idEsta;
    }

    public void setIdEsta(int idEsta) {
        this.idEsta = idEsta;
    }

    @Basic
    @Column(name = "usr_crea")
    public String getUsrCrea() {
        return usrCrea;
    }

    public void setUsrCrea(String usrCrea) {
        this.usrCrea = usrCrea;
    }

    @Basic
    @Column(name = "fch_crea")
    public Timestamp getFchCrea() {
        return fchCrea;
    }

    public void setFchCrea(Timestamp fchCrea) {
        this.fchCrea = fchCrea;
    }

    @Basic
    @Column(name = "usr_modi")
    public String getUsrModi() {
        return usrModi;
    }

    public void setUsrModi(String usrModi) {
        this.usrModi = usrModi;
    }

    @Basic
    @Column(name = "fch_modi")
    public Timestamp getFchModi() {
        return fchModi;
    }

    public void setFchModi(Timestamp fchModi) {
        this.fchModi = fchModi;
    }

    @Basic
    @Column(name = "usr_ope_crea")
    public String getUsrOpeCrea() {
        return usrOpeCrea;
    }

    public void setUsrOpeCrea(String usrOpeCrea) {
        this.usrOpeCrea = usrOpeCrea;
    }

    @Basic
    @Column(name = "usr_ope_modi")
    public String getUsrOpeModi() {
        return usrOpeModi;
    }

    public void setUsrOpeModi(String usrOpeModi) {
        this.usrOpeModi = usrOpeModi;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BidClieRegProc regProc = (BidClieRegProc) o;
        return idClie == regProc.idClie &&
                idTipo == regProc.idTipo &&
                idEsta == regProc.idEsta &&
                Objects.equals(curp, regProc.curp) &&
                Objects.equals(regIne, regProc.regIne) &&
                Objects.equals(fchRegIne, regProc.fchRegIne) &&
                Objects.equals(regFaci, regProc.regFaci) &&
                Objects.equals(fchRegFaci, regProc.fchRegFaci) &&
                Objects.equals(regDomi, regProc.regDomi) &&
                Objects.equals(fchRegDomi, regProc.fchRegDomi) &&
                Objects.equals(regDact, regProc.regDact) &&
                Objects.equals(fchRegDact, regProc.fchRegDact) &&
                Objects.equals(regCont, regProc.regCont) &&
                Objects.equals(fchRegCont, regProc.fchRegCont) &&
                Objects.equals(usrCrea, regProc.usrCrea) &&
                Objects.equals(fchCrea, regProc.fchCrea) &&
                Objects.equals(usrModi, regProc.usrModi) &&
                Objects.equals(fchModi, regProc.fchModi) &&
                Objects.equals(usrOpeCrea, regProc.usrOpeCrea) &&
                Objects.equals(usrOpeModi, regProc.usrOpeModi);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idClie, curp, regIne, fchRegIne, regFaci, fchRegFaci, regDomi, fchRegDomi, regDact, fchRegDact, regCont, fchRegCont, idTipo, idEsta, usrCrea, fchCrea, usrModi, fchModi, usrOpeCrea, usrOpeModi);
    }
}
