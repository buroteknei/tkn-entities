package com.teknei.bid.persistence.entities;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

public class BidClieNaadPK implements Serializable {
    private long idClie;
    private long idNacs;

    @Column(name = "id_clie")
    @Id
    public long getIdClie() {
        return idClie;
    }

    public void setIdClie(long idClie) {
        this.idClie = idClie;
    }

    @Column(name = "id_nacs")
    @Id
    public long getIdNacs() {
        return idNacs;
    }

    public void setIdNacs(long idNacs) {
        this.idNacs = idNacs;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BidClieNaadPK that = (BidClieNaadPK) o;
        return idClie == that.idClie &&
                idNacs == that.idNacs;
    }

    @Override
    public int hashCode() {

        return Objects.hash(idClie, idNacs);
    }
}
