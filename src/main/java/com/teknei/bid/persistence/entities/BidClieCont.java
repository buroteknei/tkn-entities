package com.teknei.bid.persistence.entities;

import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Objects;

@Entity
@Immutable
@Table(name = "bid_clie_cont", schema = "bid", catalog = "bid")
public class BidClieCont {
    private Long idClie;
    private String numeClie;  //New view
    private String nomClie;
    private String apePate;
    private String apeMate;
    private String fchNac;
    private String geneClie;
    private String lugNac;
    private String paisNac;
    private String naci;
    private String rfc;
    private String banco;
    private String ctaClabDest;
    private String aliaCta;
    private BigDecimal montMax;
    private String ctaCorr;
    private String ctaClab;
    private String domi;
    private String col;
    private String cp;
    private String muni;
    private String esta;
    private String pais;
    private String curp;
    private String emai;
    private String ciudEua;
    private String docuIden;
    private String numeDocuIden;
    //private String celular;
    //private String telCasa;
    private String numeSefe;  //New view
    private String acti;
    private String ingMensBrt;
    private String pepoExp;
    private String parePepoExp;
    private String nombPepoExp;
    private String pare;
    private String funBanc;  //New View
    private String plazCta;  //New view
    private String tasIntCta;
    private String gatNomCta;
    private String gatRealCta;
    private String plazPag;  //New view
    private String tasIntPag;
    private String gatNomPag;
    private String gatRealPag;
    private String holder;
    @Basic
    @Id
    @Column(name = "id_clie")
    public Long getIdClie() {
        return idClie;
    }

    public void setIdClie(Long idClie) {
        this.idClie = idClie;
    }

    @Basic
    @Column(name = "nume_clie")
    public String getNumeClie() {
        return numeClie;
    }

    public void setNumeClie(String numeClie) {
        this.numeClie = numeClie;
    }

    @Basic
    @Column(name = "nom_clie")
    public String getNomClie() {
        return nomClie;
    }

    public void setNomClie(String nomClie) {
        this.nomClie = nomClie;
    }

    @Basic
    @Column(name = "ape_pate")
    public String getApePate() {
        return apePate;
    }

    public void setApePate(String apePate) {
        this.apePate = apePate;
    }

    @Basic
    @Column(name = "ape_mate")
    public String getApeMate() {
        return apeMate;
    }

    public void setApeMate(String apeMate) {
        this.apeMate = apeMate;
    }

    @Basic
    @Column(name = "fch_nac")
    public String getFchNac() {
        return fchNac;
    }

    public void setFchNac(String fchNac) {
        this.fchNac = fchNac;
    }

    @Basic
    @Column(name = "gene_clie")
    public String getGeneClie() {
        return geneClie;
    }

    public void setGeneClie(String geneClie) {
        this.geneClie = geneClie;
    }

    @Basic
    @Column(name = "lug_nac")
    public String getLugNac() {
        return lugNac;
    }

    public void setLugNac(String lugNac) {
        this.lugNac = lugNac;
    }

    @Basic
    @Column(name = "pais_nac")
    public String getPaisNac() {
        return paisNac;
    }

    public void setPaisNac(String paisNac) {
        this.paisNac = paisNac;
    }

    @Basic
    @Column(name = "naci")
    public String getNaci() {
        return naci;
    }

    public void setNaci(String naci) {
        this.naci = naci;
    }

    @Basic
    @Column(name = "rfc")
    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    @Basic
    @Column(name = "banco")
    public String getBanco() {
        return banco;
    }

    public void setBanco(String banco) {
        this.banco = banco;
    }

    @Basic
    @Column(name = "cta_clab_dest")
    public String getCtaClabDest() {
        return ctaClabDest;
    }

    public void setCtaClabDest(String ctaClabDest) {
        this.ctaClabDest = ctaClabDest;
    }

    @Basic
    @Column(name = "alia_cta")
    public String getAliaCta() {
        return aliaCta;
    }

    public void setAliaCta(String aliaCta) {
        this.aliaCta = aliaCta;
    }

    @Basic
    @Column(name = "mont_max")
    public BigDecimal getMontMax() {
        return montMax;
    }

    public void setMontMax(BigDecimal montMax) {
        this.montMax = montMax;
    }

    @Basic
    @Column(name = "cta_corr")
    public String getCtaCorr() {
        return ctaCorr;
    }

    public void setCtaCorr(String ctaCorr) {
        this.ctaCorr = ctaCorr;
    }

    @Basic
    @Column(name = "cta_clab")
    public String getCtaClab() {
        return ctaClab;
    }

    public void setCtaClab(String ctaClab) {
        this.ctaClab = ctaClab;
    }

    @Basic
    @Column(name = "domi")
    public String getDomi() {
        return domi;
    }

    public void setDomi(String domi) {
        this.domi = domi;
    }

    @Basic
    @Column(name = "col")
    public String getCol() {
        return col;
    }

    public void setCol(String col) {
        this.col = col;
    }

    @Basic
    @Column(name = "cp")
    public String getCp() {
        return cp;
    }

    public void setCp(String cp) {
        this.cp = cp;
    }

    @Basic
    @Column(name = "muni")
    public String getMuni() {
        return muni;
    }

    public void setMuni(String muni) {
        this.muni = muni;
    }

    @Basic
    @Column(name = "esta")
    public String getEsta() {
        return esta;
    }

    public void setEsta(String esta) {
        this.esta = esta;
    }

    @Basic
    @Column(name = "pais")
    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    @Basic
    @Column(name = "curp")
    public String getCurp() {
        return curp;
    }

    public void setCurp(String curp) {
        this.curp = curp;
    }

    @Basic
    @Column(name = "emai")
    public String getEmai() {
        return emai;
    }

    public void setEmai(String emai) {
        this.emai = emai;
    }

    @Basic
    @Column(name = "ciud_eua")
    public String getCiudEua() {
        return ciudEua;
    }

    public void setCiudEua(String ciudEua) {
        this.ciudEua = ciudEua;
    }

    @Basic
    @Column(name = "docu_iden")
    public String getDocuIden() {
        return docuIden;
    }

    public void setDocuIden(String docuIden) {
        this.docuIden = docuIden;
    }

    @Basic
    @Column(name = "nume_docu_iden")
    public String getNumeDocuIden() {
        return numeDocuIden;
    }

    public void setNumeDocuIden(String numeDocuIden) {
        this.numeDocuIden = numeDocuIden;
    }

    @Basic
    @Column(name = "nume_sefe")
    public String getNumeSefe() {
        return numeSefe;
    }

    public void setNumeSefe(String numeSefe) {
        this.numeSefe = numeSefe;
    }

    @Basic
    @Column(name = "acti")
    public String getActi() {
        return acti;
    }

    public void setActi(String acti) {
        this.acti = acti;
    }

    @Basic
    @Column(name = "ing_mens_brt")
    public String getIngMensBrt() {
        return ingMensBrt;
    }

    public void setIngMensBrt(String ingMensBrt) {
        this.ingMensBrt = ingMensBrt;
    }

    @Basic
    @Column(name = "pepo_exp")
    public String getPepoExp() {
        return pepoExp;
    }

    public void setPepoExp(String pepoExp) {
        this.pepoExp = pepoExp;
    }

    @Basic
    @Column(name = "pare_pepo_exp")
    public String getParePepoExp() {
        return parePepoExp;
    }

    public void setParePepoExp(String parePepoExp) {
        this.parePepoExp = parePepoExp;
    }

    @Basic
    @Column(name = "nomb_pepo_exp")
    public String getNombPepoExp() {
        return nombPepoExp;
    }

    public void setNombPepoExp(String nombPepoExp) {
        this.nombPepoExp = nombPepoExp;
    }

    @Basic
    @Column(name = "pare")
    public String getPare() {
        return pare;
    }

    public void setPare(String pare) {
        this.pare = pare;
    }

    @Basic
    @Column(name = "func_banc")
    public String getFunBanc() {
        return funBanc;
    }

    public void setFunBanc(String funBanc) {
        this.funBanc = funBanc;
    }

    @Basic
    @Column(name = "plaz_cta")
    public String getPlazCta() {
        return plazCta;
    }

    public void setPlazCta(String plazCta) {
        this.plazCta = plazCta;
    }

    @Basic
    @Column(name = "tas_int_cta")
    public String getTasIntCta() {
        return tasIntCta;
    }

    public void setTasIntCta(String tasIntCta) {
        this.tasIntCta = tasIntCta;
    }

    @Basic
    @Column(name = "gat_nom_cta")
    public String getGatNomCta() {
        return gatNomCta;
    }

    public void setGatNomCta(String gatNomCta) {
        this.gatNomCta = gatNomCta;
    }

    @Basic
    @Column(name = "gat_real_cta")
    public String getGatRealCta() {
        return gatRealCta;
    }

    public void setGatRealCta(String gatRealCta) {
        this.gatRealCta = gatRealCta;
    }

    @Basic
    @Column(name = "plaz_pag")
    public String getPlazPag() {
        return plazPag;
    }

    public void setPlazPag(String plazPag) {
        this.plazPag = plazPag;
    }

    @Basic
    @Column(name = "tas_int_pag")
    public String getTasIntPag() {
        return tasIntPag;
    }

    public void setTasIntPag(String tasIntPag) {
        this.tasIntPag = tasIntPag;
    }

    @Basic
    @Column(name = "gat_nom_pag")
    public String getGatNomPag() {
        return gatNomPag;
    }

    public void setGatNomPag(String gatNomPag) {
        this.gatNomPag = gatNomPag;
    }

    @Basic
    @Column(name = "gat_real_pag")
    public String getGatRealPag() {
        return gatRealPag;
    }

    public void setGatRealPag(String gatRealPag) {
        this.gatRealPag = gatRealPag;
    }

    @Basic
    @Column(name = "holder")
    public String getHolder() {
		return holder;
	}

	public void setHolder(String holder) {
		this.holder = holder;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BidClieCont that = (BidClieCont) o;
        return Objects.equals(idClie, that.idClie) &&
                Objects.equals(nomClie, that.nomClie) &&
                Objects.equals(apePate, that.apePate) &&
                Objects.equals(apeMate, that.apeMate) &&
                Objects.equals(fchNac, that.fchNac) &&
                Objects.equals(geneClie, that.geneClie) &&
                Objects.equals(lugNac, that.lugNac) &&
                Objects.equals(paisNac, that.paisNac) &&
                Objects.equals(naci, that.naci) &&
                Objects.equals(rfc, that.rfc) &&
                Objects.equals(banco, that.banco) &&
                Objects.equals(ctaClabDest, that.ctaClabDest) &&
                Objects.equals(aliaCta, that.aliaCta) &&
                Objects.equals(montMax, that.montMax) &&
                Objects.equals(ctaCorr, that.ctaCorr) &&
                Objects.equals(ctaClab, that.ctaClab) &&
                Objects.equals(domi, that.domi) &&
                Objects.equals(col, that.col) &&
                Objects.equals(cp, that.cp) &&
                Objects.equals(muni, that.muni) &&
                Objects.equals(esta, that.esta) &&
                Objects.equals(pais, that.pais) &&
                Objects.equals(curp, that.curp) &&
                Objects.equals(emai, that.emai) &&
                Objects.equals(ciudEua, that.ciudEua) &&
                Objects.equals(docuIden, that.docuIden) &&
                Objects.equals(numeDocuIden, that.numeDocuIden) &&
                // Objects.equals(celular, that.celular) &&
                // Objects.equals(telCasa, that.telCasa) &&
                Objects.equals(acti, that.acti) &&
                Objects.equals(ingMensBrt, that.ingMensBrt) &&
                Objects.equals(pepoExp, that.pepoExp) &&
                Objects.equals(parePepoExp, that.parePepoExp) &&
                Objects.equals(nombPepoExp, that.nombPepoExp) &&
                Objects.equals(pare, that.pare) &&
                Objects.equals(tasIntCta, that.tasIntCta) &&
                Objects.equals(gatNomCta, that.gatNomCta) &&
                Objects.equals(gatRealCta, that.gatRealCta) &&
                Objects.equals(tasIntPag, that.tasIntPag) &&
                Objects.equals(gatNomPag, that.gatNomPag) &&
                Objects.equals(gatRealPag, that.gatRealPag);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idClie, nomClie, apePate, apeMate, fchNac, geneClie, lugNac, paisNac, naci, rfc, banco, ctaClabDest, aliaCta, montMax, ctaCorr, ctaClab, domi, col, cp, muni, esta, pais, curp, emai, ciudEua, docuIden, numeDocuIden, acti, ingMensBrt, pepoExp, parePepoExp, nombPepoExp, pare, tasIntCta, gatNomCta, gatRealCta, tasIntPag, gatNomPag, gatRealPag);
    }

	@Override
	public String toString() {
		return "BidClieCont [idClie=" + idClie + ", numeClie=" + numeClie + ", nomClie=" + nomClie + ", apePate="
				+ apePate + ", apeMate=" + apeMate + ", fchNac=" + fchNac + ", geneClie=" + geneClie + ", lugNac="
				+ lugNac + ", paisNac=" + paisNac + ", naci=" + naci + ", rfc=" + rfc + ", banco=" + banco
				+ ", ctaClabDest=" + ctaClabDest + ", aliaCta=" + aliaCta + ", montMax=" + montMax + ", ctaCorr="
				+ ctaCorr + ", ctaClab=" + ctaClab + ", domi=" + domi + ", col=" + col + ", cp=" + cp + ", muni=" + muni
				+ ", esta=" + esta + ", pais=" + pais + ", curp=" + curp + ", emai=" + emai + ", ciudEua=" + ciudEua
				+ ", docuIden=" + docuIden + ", numeDocuIden=" + numeDocuIden + ", numeSefe=" + numeSefe + ", acti="
				+ acti + ", ingMensBrt=" + ingMensBrt + ", pepoExp=" + pepoExp + ", parePepoExp=" + parePepoExp
				+ ", nombPepoExp=" + nombPepoExp + ", pare=" + pare + ", funBanc=" + funBanc + ", plazCta=" + plazCta
				+ ", tasIntCta=" + tasIntCta + ", gatNomCta=" + gatNomCta + ", gatRealCta=" + gatRealCta + ", plazPag="
				+ plazPag + ", tasIntPag=" + tasIntPag + ", gatNomPag=" + gatNomPag + ", gatRealPag=" + gatRealPag
				+ ", holder=" + holder + "]";
	}
    
    
}