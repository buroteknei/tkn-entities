package com.teknei.bid.persistence.entities;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

public class BidClieDactPK implements Serializable {
    private long idClie;
    private long idDact;

    @Column(name = "id_clie")
    @Id
    public long getIdClie() {
        return idClie;
    }

    public void setIdClie(long idClie) {
        this.idClie = idClie;
    }

    @Column(name = "id_dact")
    @Id
    public long getIdDact() {
        return idDact;
    }

    public void setIdDact(long idDact) {
        this.idDact = idDact;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BidClieDactPK that = (BidClieDactPK) o;
        return idClie == that.idClie &&
                idDact == that.idDact;
    }

    @Override
    public int hashCode() {

        return Objects.hash(idClie, idDact);
    }
}
