package com.teknei.bid.persistence.entities;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "bid_otp", schema = "bid", catalog = "bid")
public class BidOtp {
    private long idRegi;
    private Long idClie;
    private String otp;
    private boolean used;
    private Boolean send;
    private Timestamp fchSend;
    private int idTipo;
    private int idEsta;
    private String usrCrea;
    private Timestamp fchCrea;
    private String usrModi;
    private Timestamp fchModi;
    private String usrOpeCrea;
    private String usrOpeModi;

    @Id
    @SequenceGenerator(schema = "bid", sequenceName = "bid.bid_otp_id_regi_seq", allocationSize = 1, name = "bid_otp_gen", catalog = "bid_db")
    @GeneratedValue(generator = "bid_otp_gen", strategy = GenerationType.SEQUENCE)
    @Column(name = "id_regi")
    public long getIdRegi() {
        return idRegi;
    }

    public void setIdRegi(long idRegi) {
        this.idRegi = idRegi;
    }

    @Basic
    @Column(name = "id_clie")
    public Long getIdClie() {
        return idClie;
    }

    public void setIdClie(Long idClie) {
        this.idClie = idClie;
    }

    @Basic
    @Column(name = "otp")
    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    @Basic
    @Column(name = "used")
    public boolean isUsed() {
        return used;
    }

    public void setUsed(boolean used) {
        this.used = used;
    }

    @Basic
    @Column(name = "send")
    public Boolean getSend() {
        return send;
    }

    public void setSend(Boolean send) {
        this.send = send;
    }

    @Basic
    @Column(name = "fch_send")
    public Timestamp getFchSend() {
        return fchSend;
    }

    public void setFchSend(Timestamp fchSend) {
        this.fchSend = fchSend;
    }

    @Basic
    @Column(name = "id_tipo")
    public int getIdTipo() {
        return idTipo;
    }

    public void setIdTipo(int idTipo) {
        this.idTipo = idTipo;
    }

    @Basic
    @Column(name = "id_esta")
    public int getIdEsta() {
        return idEsta;
    }

    public void setIdEsta(int idEsta) {
        this.idEsta = idEsta;
    }

    @Basic
    @Column(name = "usr_crea")
    public String getUsrCrea() {
        return usrCrea;
    }

    public void setUsrCrea(String usrCrea) {
        this.usrCrea = usrCrea;
    }

    @Basic
    @Column(name = "fch_crea")
    public Timestamp getFchCrea() {
        return fchCrea;
    }

    public void setFchCrea(Timestamp fchCrea) {
        this.fchCrea = fchCrea;
    }

    @Basic
    @Column(name = "usr_modi")
    public String getUsrModi() {
        return usrModi;
    }

    public void setUsrModi(String usrModi) {
        this.usrModi = usrModi;
    }

    @Basic
    @Column(name = "fch_modi")
    public Timestamp getFchModi() {
        return fchModi;
    }

    public void setFchModi(Timestamp fchModi) {
        this.fchModi = fchModi;
    }

    @Basic
    @Column(name = "usr_ope_crea")
    public String getUsrOpeCrea() {
        return usrOpeCrea;
    }

    public void setUsrOpeCrea(String usrOpeCrea) {
        this.usrOpeCrea = usrOpeCrea;
    }

    @Basic
    @Column(name = "usr_ope_modi")
    public String getUsrOpeModi() {
        return usrOpeModi;
    }

    public void setUsrOpeModi(String usrOpeModi) {
        this.usrOpeModi = usrOpeModi;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BidOtp bidOtp = (BidOtp) o;
        return idRegi == bidOtp.idRegi &&
                used == bidOtp.used &&
                idTipo == bidOtp.idTipo &&
                idEsta == bidOtp.idEsta &&
                Objects.equals(idClie, bidOtp.idClie) &&
                Objects.equals(otp, bidOtp.otp) &&
                Objects.equals(send, bidOtp.send) &&
                Objects.equals(fchSend, bidOtp.fchSend) &&
                Objects.equals(usrCrea, bidOtp.usrCrea) &&
                Objects.equals(fchCrea, bidOtp.fchCrea) &&
                Objects.equals(usrModi, bidOtp.usrModi) &&
                Objects.equals(fchModi, bidOtp.fchModi) &&
                Objects.equals(usrOpeCrea, bidOtp.usrOpeCrea) &&
                Objects.equals(usrOpeModi, bidOtp.usrOpeModi);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idRegi, idClie, otp, used, send, fchSend, idTipo, idEsta, usrCrea, fchCrea, usrModi, fchModi, usrOpeCrea, usrOpeModi);
    }
}
