package com.teknei.bid.persistence.entities;

import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Immutable
@Table(name = "bid_clie_rfis_cont", schema = "bid", catalog = "bid")
public class BidClieRfisCont {
    private Long idClie;
    private String nomPais;
    private String tin;
    private String razNoTin;
    private String myKey;

    @Basic
    @Column(name = "my_key")
    @Id
    public String getMyKey() {
        return myKey;
    }

    public void setMyKey(String myKey) {
        this.myKey = myKey;
    }

    @Basic
    @Column(name = "id_clie")
    public Long getIdClie() {
        return idClie;
    }

    public void setIdClie(Long idClie) {
        this.idClie = idClie;
    }

    @Basic
    @Column(name = "nom_pais")
    public String getNomPais() {
        return nomPais;
    }

    public void setNomPais(String nomPais) {
        this.nomPais = nomPais;
    }

    @Basic
    @Column(name = "tin")
    public String getTin() {
        return tin;
    }

    public void setTin(String tin) {
        this.tin = tin;
    }

    @Basic
    @Column(name = "raz_no_tin")
    public String getRazNoTin() {
        return razNoTin;
    }

    public void setRazNoTin(String razNoTin) {
        this.razNoTin = razNoTin;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BidClieRfisCont that = (BidClieRfisCont) o;
        return Objects.equals(idClie, that.idClie) &&
                Objects.equals(nomPais, that.nomPais) &&
                Objects.equals(tin, that.tin) &&
                Objects.equals(razNoTin, that.razNoTin);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idClie, nomPais, tin, razNoTin);
    }
}
