package com.teknei.bid.persistence.entities;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

public class BidClieMailPK implements Serializable {
    private long idClie;
    private long idEmai;

    @Column(name = "id_clie")
    @Id
    public long getIdClie() {
        return idClie;
    }

    public void setIdClie(long idClie) {
        this.idClie = idClie;
    }

    @Column(name = "id_emai")
    @Id
    public long getIdEmai() {
        return idEmai;
    }

    public void setIdEmai(long idEmai) {
        this.idEmai = idEmai;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BidClieMailPK that = (BidClieMailPK) o;
        return idClie == that.idClie &&
                idEmai == that.idEmai;
    }

    @Override
    public int hashCode() {

        return Objects.hash(idClie, idEmai);
    }
}
