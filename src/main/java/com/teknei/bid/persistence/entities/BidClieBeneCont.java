package com.teknei.bid.persistence.entities;

import org.hibernate.annotations.Immutable;
import org.hibernate.annotations.ListIndexBase;

import javax.persistence.*;
import java.math.BigInteger;
import java.util.Objects;

@Entity
@Immutable
@Table(name = "bid_clie_bene_cont", schema = "bid", catalog = "bid")
public class BidClieBeneCont {
    private Long idClie;
    private String descPare;
    private String nombBene;
    private String fchNac;
    private String domiBene;
    private BigInteger porcPart;
    private String myKey;

    @Basic
    @Column(name = "my_key")
    @Id
    public String getMyKey() {
        return myKey;
    }

    public void setMyKey(String myKey) {
        this.myKey = myKey;
    }

    @Basic
    @Column(name = "id_clie")
    public Long getIdClie() {
        return idClie;
    }

    public void setIdClie(Long idClie) {
        this.idClie = idClie;
    }

    @Basic
    @Column(name = "desc_pare")
    public String getDescPare() {
        return descPare;
    }

    public void setDescPare(String descPare) {
        this.descPare = descPare;
    }

    @Basic
    @Column(name = "nomb_bene")
    public String getNombBene() {
        return nombBene;
    }

    public void setNombBene(String nombBene) {
        this.nombBene = nombBene;
    }

    @Basic
    @Column(name = "fch_nac")
    public String getFchNac() {
        return fchNac;
    }

    public void setFchNac(String fchNac) {
        this.fchNac = fchNac;
    }

    @Basic
    @Column(name = "domi_bene")
    public String getDomiBene() {
        return domiBene;
    }

    public void setDomiBene(String domiBene) {
        this.domiBene = domiBene;
    }

    @Basic
    @Column(name = "porc_part")
    public BigInteger getPorcPart() {
        return porcPart;
    }

    public void setPorcPart(BigInteger porcPart) {
        this.porcPart = porcPart;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BidClieBeneCont that = (BidClieBeneCont) o;
        return Objects.equals(idClie, that.idClie) &&
                Objects.equals(descPare, that.descPare) &&
                Objects.equals(nombBene, that.nombBene) &&
                Objects.equals(fchNac, that.fchNac) &&
                Objects.equals(domiBene, that.domiBene) &&
                Objects.equals(porcPart, that.porcPart);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idClie, descPare, nombBene, fchNac, domiBene, porcPart);
    }
}
